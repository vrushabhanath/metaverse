using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit.Inputs;
public class handController : MonoBehaviour
{
    public InputActionProperty _pinchAction;
    public InputActionProperty _gripAction;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float _pinchValue = _pinchAction.action.ReadValue<float>();
        float _gripValue = _gripAction.action.ReadValue<float>();

        animator.SetFloat("Grip", _gripValue);
        animator.SetFloat("Trigger" ,_pinchValue);
        
    }
}
